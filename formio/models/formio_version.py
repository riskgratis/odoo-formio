# Copyright Nova Code (http://www.novacode.nl)
# See LICENSE file for full licensing details.

from pkg_resources import parse_version

from odoo import api, fields, models


class Version(models.Model):
    _name = 'formio.version'
    _description = 'formio.js Version'
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'sequence DESC'

    name = fields.Char(
        "Name", required=True, tracking=True,
=======
=======
>>>>>>> upstream/12.0
    _order = 'sequence DESC'

    name = fields.Char(
        "Name", required=True, track_visibility='onchange',
<<<<<<< HEAD
>>>>>>> upstream/13.0
=======
>>>>>>> upstream/12.0
        help="""formio.js release/version.""")
    sequence = fields.Integer()
=======
    _order = 'name DESC'

    name = fields.Char(
        "Name", required=True, track_visibility='onchange',
        help="""formio.js release/version.""")
>>>>>>> upstream/11.0
    description = fields.Text("Description")
    translations = fields.Many2many('formio.translation', string='Translations')
    assets = fields.One2many('formio.version.asset', 'version_id', string='Assets (js, css)', domain=[('type', 'in', ['css', 'js'])])
    css_assets = fields.One2many(
        'formio.version.asset', 'version_id', domain=[('type', '=', 'css')], copy=True,
        string='CSS Assets')
    js_assets = fields.One2many(
        'formio.version.asset', 'version_id', domain=[('type', '=', 'js')], copy=True,
        string='Javascript Assets')
    license_assets = fields.One2many(
<<<<<<< HEAD
        'formio.version.asset', 'version_id',  domain=[('type', '=', 'license')], copy=True,
        string='License Assets')

    def unlink(self):
        self.assets.unlink()
=======
        'formio.version.asset', 'version_id',  domain=[('type', '=', 'license')],
        string='License Assets')

    def unlink(self):
>>>>>>> upstream/12.0
        domain = [('formio_version_id', 'in', self.ids)]
        self.env['formio.version.github.tag'].search(domain).write({'state': 'available'})
        return super(Version, self).unlink()

    @api.model
    def create(self, vals):
        res = super(Version, self).create(vals)
        self._update_versions_sequence()
        return res

<<<<<<< HEAD
=======
    @api.multi
>>>>>>> upstream/12.0
    def write(self, vals):
        res = super(Version, self).write(vals)
        if 'name' in vals:
            self._update_versions_sequence()
        return res

    @api.model
    def _update_versions_sequence(self):
        versions = self.search([])
        names = versions.mapped('name')
        names = sorted(names, key=parse_version)
        seq = 0
        for name in names:
            seq += 1
            version = versions.filtered(lambda r: r.name == name)[0]
            version.sequence = seq
        
